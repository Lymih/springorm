package co.simplon.promo18.springorm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.simplon.promo18.springorm.entity.Dog;

@Repository
public interface DogRepository extends JpaRepository<Dog,Integer> {
  
}
