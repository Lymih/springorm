package co.simplon.promo18.springorm.controller;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.simplon.promo18.springorm.entity.Contest;
import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.repository.ContestRepository;

@RestController
@RequestMapping("/api/contest")
public class ContestController {

  @Autowired
  private ContestRepository repo;

  @GetMapping
  public List <Contest> getAll(){
    return repo.findAll();
    
  }

  @GetMapping("/{id}/dog")
  public Set<Dog> getDogByContest(@PathVariable int id){
    Contest contest = repo.findById(id).get();
    return contest.getDogs();
  }

  
}
