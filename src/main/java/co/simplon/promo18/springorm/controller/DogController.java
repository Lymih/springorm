package co.simplon.promo18.springorm.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.repository.DogRepository;

@RestController
@RequestMapping("/api/dog")
public class DogController {
  @Autowired
  private DogRepository repo;

  @GetMapping
  public List<Dog> getAll() {
    return repo.findAll();
  }

  @GetMapping("/{id}")
  public Dog getOne(@PathVariable int id) {
    return repo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Dog add(@Valid @RequestBody Dog dog) {
    dog.setId(null);
    return repo.save(dog);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable int id) {
    Dog dogToDelete = getOne(id);
    repo.delete(dogToDelete);
  }

  @PutMapping("/{id}")
  public Dog update(@PathVariable int id,@Valid @RequestBody Dog dog) {
    Dog dogToUpdate = getOne(id);
    dogToUpdate.setName(dog.getName());
    dogToUpdate.setBreed(dog.getBreed());
    dogToUpdate.setBirthDate(dog.getBirthDate());
    return repo.save(dogToUpdate);
  }
}
