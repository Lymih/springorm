package co.simplon.promo18.springorm.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Dog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotBlank
  private String name;

  private String Breed;

  @PastOrPresent
  private LocalDate birthDate;
  
  @ManyToOne
  @JsonIgnoreProperties("dogs")
  private Trainer trainer;

  @ManyToMany
  @JsonIgnore
  private Set<Contest> contests = new HashSet<>();

  public Trainer getTrainer() {
    return trainer;
  }

  public void setTrainer(Trainer trainer) {
    this.trainer = trainer;
  }

  public Dog() {}

  public Dog(String name, String breed, LocalDate birthDate) {
    this.name = name;
    Breed = breed;
    this.birthDate = birthDate;
  }

  public Dog(Integer id, String name, String breed, LocalDate birthDate) {
    this.id = id;
    this.name = name;
    Breed = breed;
    this.birthDate = birthDate;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getBreed() {
    return Breed;
  }

  public LocalDate getBirthDate() {
    return birthDate;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setBreed(String breed) {
    Breed = breed;
  }

  public void setBirthDate(LocalDate birthDate) {
    this.birthDate = birthDate;
  }

  
}
